#!/usr/bin/env python	
from __future__ import with_statement
import os
import sys
from sys import stdin, stderr, stdout
import argparse
#from subprocess import Popen, PIPE
#from collections import defaultdict
#import matplotlib.pyplot as plt

from ete3 import Tree
import random
import string

def main():
	usage = ("%prog LEAVES_NUM >TREE.newick \
		Generate a random newick tree (using ete3's populate) ")

	parser = argparse.ArgumentParser(usage=usage)
	parser.add_argument('leaves_num', type=int, help='number of leaves in generated tree')
	parser.add_argument('-b', '--branch_range', type=str, default=False, 
			help='custom branch length range, values should be comma separated [default range: 0.01,2]')
	args = parser.parse_args()
	
	nodes_num = args.leaves_num * 2 
	symbols = string.ascii_letters
	# a list of node names
	nodes_names = [''.join([random.choice(symbols) for i in range(5)]) for j in range(nodes_num)]
	if args.branch_range:
		branch_range = tuple([float(x) for x in args.branch_range.split(",")])
	else:
		branch_range = (0.01, 2)

	tree = Tree()
	# generate a tree
	tree.populate(args.leaves_num, random_branches=True, branch_range=branch_range)
	
	# name all nodes (populate only names leaves)
	i = 0
	for node in tree.traverse("preorder"):
		node.name = nodes_names[i]
		i = i+1
	# root tree, name root node
	tree.get_tree_root().name = "ROOT"

	# print it in newick format, include internal node names
	print(tree.write(format=1))

if __name__ == '__main__':
	main()

