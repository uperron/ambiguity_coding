#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr, stdout
import argparse

import numpy as np
import pandas as pd
from dendropy import Tree, TaxonNamespace, Taxon

def main():
	parser = argparse.ArgumentParser(description="Compare reconstructed sequences to the reference")
	parser.add_argument('ref_alignfile', nargs=None, type=str, help="reference alignment of internal sequences")
	parser.add_argument('rec_alignfile', nargs=None, type=str, help="reconstructed alignment of internal sequences")
	parser.add_argument('-t', '--tree', default=False, help='topology-based analysis, requires treefile,full_ref_align (including terminal nodes)')	
	args = parser.parse_args()


	def split_PHYLIP(filename):
		# split sequences into alignment columns, one per site
		df = pd.read_csv(filename,  sep="\t", header=None, index_col=0, names=["sequence"], skiprows=1)
		split_df = df["sequence"].str.split(' ', expand=True).rename(columns = lambda x: str(x+1))
		return split_df
	
	def identity(seq1, seq2):
		return sum(1 for a, b in zip(seq1, seq2) if a == b)

	if args.tree:
		treefile, leaf_alignfile = options.tree.split(",")
		tns = TaxonNamespace(is_case_sensitive=True)
		tree = Tree.get(path=treefile, schema='newick', taxon_namespace=tns, case_sensitive_taxon_labels=True, suppress_internal_node_taxa=False)
		leaf_align_df = split_PHYLIP(leaf_alignfile)
		ref_align_df = split_PHYLIP(args[0])
		ref_align_df = ref_align_df.append(leaf_align_df)
	else:
		ref_align_df = split_PHYLIP(args.ref_alignfile)
	align_len = int(ref_align_df.columns[-1])
	
	rec_align_df = split_PHYLIP(args.rec_alignfile)
	if options.tree:
		out_dict = {}
		# add leaf nodes to recountructed alignment
		rec_align_df = rec_align_df.append(leaf_align_df)
		# iterate over internal nodes
		for node in tree.postorder_internal_node_iter():
			if node._parent_node is None:
				nodename = "ROOT"
			else:
				nodename = node.taxon.label
			rec_nodeseq = rec_align_df.loc[nodename].values
			ref_nodeseq = ref_align_df.loc[nodename].values	
			child_1, child_2 = [childnode.taxon.label for childnode in node.child_node_iter()]
			# branch lengths
			brlen_1, brlen_2 = [childnode.edge_length for childnode in node.child_node_iter()]

			# % identity of children nodes' ref sequence vs current node's reference sequence
			idchild_1, idchild_2 = [100. * identity(ref_nodeseq, ref_align_df.loc[child].values) / align_len for child in [child_1, child_2]]
			# % identity of children nodes' reconstructed (or leaf) sequence vs current node's reconstructed sequence
			id_rec_child_1, id_rec_child_2 = [100. * identity(rec_nodeseq, rec_align_df.loc[x].tolist()) / align_len for x in [child_1, child_2]]

			# number of sites that disagree (i.e. have different states) in this node's children reconstructed (or leaf) sequences
			dis = sum([1 for site in ref_align_df.columns.values if rec_align_df[site][child_1] != rec_align_df[site][child_2]])
			if dis == 0:
				dis_correct = 100.
			else:
				# % of sites that disagree in children's reconstructed (or leaf) sequences and are reconstructed correctly in current node's sequence
				dis_correct = 100. * sum([1 for site in ref_align_df.columns.values if rec_align_df[site][child_1] != rec_align_df[site][child_2] and rec_align_df[site][nodename] == ref_align_df[site][nodename]]) / dis
			out_dict[nodename] = [child_1, child_2, brlen_1, brlen_2 , idchild_1, idchild_2, id_rec_child_1, id_rec_child_2, dis_correct]
		# in order to only print internal nodes
		ref_align_df = split_PHYLIP(args[0])
		rec_align_df = split_PHYLIP(filename)

	int_taxons = ref_align_df.index.values
	for filename, taxon, score in zip([filename]*len(int_taxons), int_taxons, [100. * np.sum(x) / align_len for x in np.vectorize(identity)(ref_align_df, rec_align_df)]):
		if options.tree:
			out = '\t'.join(map(str, out_dict[taxon]))
			print "%s\t%s\t%f\t%s" % (filename, taxon, score, out)
		else: 
			print "%s\t%s\t%f" % (filename, taxon, score)

if __name__ == '__main__':
	main()

