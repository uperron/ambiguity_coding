#!/usr/bin/env python
import sys
from sys import stdin, stderr, stdout
import argparse

import re

def main():
	usage = '''%prog SCALING_FACTOR_1 [...]  < NEWICK_FILE 
		
		scale branch lengths according to a a given factor leaving tree topology unchanged'''
	parser = argparse.ArgumentParser(usage=usage)
	parser.add_argument("scaling_factor", type=float)
	args = parser.parse_args()

	for line in stdin:
		tree_string = line.rstrip()
		# assumes branch lengths to be floats
		distances = re.findall(r'\d+\.\d+', tree_string)
		for distance in distances:
			scaled_dist = str(float(distance) * args.scaling_factor)
			tree_string = tree_string.replace(distance, scaled_dist)
		print(tree_string)

if __name__ == '__main__':
	main()

