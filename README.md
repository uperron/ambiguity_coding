## Supplementary Material
Ambiguity coding allows accurate inference of evolutionary parameters from alignments in an aggregated state-space, Weber et al. 2019. 

### ancestral_lin-marginal_reconstruct.py
Performs ancestral sequence (rotasequence) reconstruction using a phylogentic tree,
a replacement model and the marginal reconstruction algorithm (Koshi et al. 1996).

### ancestral_joint_reconstruct.py
Performs ancestral sequence (rotasequence) reconstruction using a phylogentic tree,
a replacement model and the marginal reconstruction algorithm (Pupko et al. 2000).

### compare_reconstructed_seqs.py
Computes reconstruction accuracy

### scale_tree.py
Scales tree branch lengths

### tree_gen.py
Generates random phylogenies

### tree_sim.py
Generates a rotaseuence  alignment by performing a continuous-time Markov chain simulation 
along their branches according to a model's exchange rates (Perron et al. 2019).

### RAM55_1-char_state_encoding.input
Describes how amino acid and rotamer states (i.e. a mixed alignment) are encoded into likelihood vectors.
The first row reports the number of legal characters and the number of internal states in the model (here 55 for RAM55).
The second row reports all legal characters. 
The third row shows labels for the model's internal states (for illustrative purposes).
The following rows map each legal character to a likelihood vector.

### RAM55.exchangeabilities.tsv, RAM55.frequencies.tsv
RAM55 exchangeabilities and frequencies as used by ancestral_lin-marginal_reconstruct.py and ancestral_joint_reconstruct.py

### RAM55.exchangeabilities.ng, RAM55.frequencies.ng
RAM55 exchangeabilities and frequencies as used by RAxML-NG (Kozlov et al. 2019, https://github.com/amkozlov/raxml-ng) to estimate a phylogeny from an alignment of rotamer sequences.
e.g.: $raxml-ng --msa {rubisco_empiricalx31x500_1-char.rotasequences.phylip} --model MULTI55_GTR{RAM55.exchangeabilities.ng}+FU{RAM55.frequencies.ng}+M{RAM55_1-char_state_encoding.input} --seed 1 --threads 1

### rubisco_empirical.tsv
Details for protein structures in the RuBisCO dataset.

### ADK_empirical.tsv
Details for protein structures in the ADK dataset.

### rubisco_empiricalx31x500_1-char.rotasequences.phylip
Rotamer sequence alignment for the RuBisCO dataset, see Perron et al. 2019 for details on how rotamer states are 
assigned.

### ADK_empiricalx16x271_1-char.rotasequences.phylip
Rotamer sequence alignment for the ADK dataset.
