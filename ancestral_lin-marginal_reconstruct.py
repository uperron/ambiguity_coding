#!/usr/bin/env python

from sys import stdin, stderr
import argparse
import numpy as np
from ruamel.yaml import YAML
yaml = YAML()

from collections import defaultdict, OrderedDict
import numpy as np
import scipy.linalg
import pandas as pd
from dendropy import Tree, TaxonNamespace, Taxon
import random
import string
import warnings
import itertools
import json

def main():
	usage = "Reconstruct internal node rotasequences using the Marginal  Reconstruction algorithm from Koshi and Goldstein 1996 DOI: 10.1007/BF02198858"

	parser = argparse.ArgumentParser(description=usage)
	parser.add_argument('treefile', nargs=None, type=str, help=".newick phylogeny to be used for reconstruction")
	parser.add_argument('alignfile',  nargs=None, type=str, help=".PHYLIP alignment (rotasequences, AA sequences or both) to be used for reconstruction")
	parser.add_argument('IRMfile',  nargs=None, type=str, help="scaled Q_matrix (1 AA substitution per unit of time)")
	parser.add_argument('freqfile',  nargs=None, type=str, help="state equilibrium frequencies)")
	parser.add_argument('dictfile',  nargs=None, type=str, help="dictionary file)")
	parser.add_argument('-e', '--exhaustive', action="store_true", default=False, help='Iter over all states not only those observed at site')
	parser.add_argument('-A', '--AA_model', action="store_true", default=False, help='use AA states as definite states in aq 20-state model')
	parser.add_argument('-i', '--ignore_leaf', type=str, default=False, help='Ignore leaf sequence (i.e. treat leaf as internal node)')
	parser.add_argument('-I', '--Ignore_and_trim_sibling', type=str, default=False, help='Ignore leaf sequence and do not use Lx from its sibling (i.e. treat leaf as root)')
	parser.add_argument('-p', '--print_posterior',  type=str,  default=False, help='Print to file the posterior probability vector for all internal nodes')
	parser.add_argument('-H', '--Homology', type=str, default=False, help='Reconstruct rotamer config for taxon, constrained on observed AA')
	args = parser.parse_args()
	

	# load state dictionaries
	with open(args.dictfile, "r") as infile:
		data = yaml.load(infile)
	masking_dict = dict(data['MASKING_DICT'])
	ambiguity_dict = dict(data['AMBIGUITY_DICT'])
	ambiguous_states = list(data['AA_STATES'])
	definite_states = list(data['ROTA_STATES'])
	
	if args.AA_model:
		definite_states = list(data['AA_STATES'])
		ambiguous_states = []

	if args.Ignore_and_trim_sibling:
		args.ignore_leaf = args.Ignore_and_trim_sibling
	
	tns = TaxonNamespace(is_case_sensitive=True)

	# dedropy creates a fake root if tree is unrooted
	tree = Tree.get(path=args.treefile, schema='newick', taxon_namespace=tns, case_sensitive_taxon_labels=True, suppress_internal_node_taxa=False)

	# assign taxon instances (and corresponding randomly-generated labels) 
	# to internal nodes if not present
	taxonless_intnodes = [intnode for intnode in tree.postorder_internal_node_iter() if intnode.taxon is None and intnode._parent_node is not None]
	if len(taxonless_intnodes) >0:
		warnings.warn('Internal node taxon labels not found in %s, generating random labels...' % (args.treefile))		
		symbols = string.ascii_letters
		intnode_names = [''.join([random.choice(symbols) for i in range(5)]) for j in range(len(taxonless_intnodes))]
		for intnode, intnode_name in zip(taxonless_intnodes, intnode_names):
			intnode.taxon = Taxon(label=intnode_name)
			warnings.warn('Assigned label %s to node %s' % (intnode.taxon.label, str(intnode)))
		root = [intnode for intnode in tree.postorder_internal_node_iter() if intnode._parent_node is None][0]
		root.taxon = Taxon(label='ROOT')
		warnings.warn('Assigned label %s to node %s' % (root.taxon.label, str(root)))
	internal_taxons = ['ROOT'] + [intnode.taxon.label for intnode in tree.postorder_internal_node_iter() if intnode._parent_node is not None]	

	# read input alignment
	orig_align_df = pd.read_csv(args.alignfile,  sep="\t", header=None, index_col=0, names=["sequence"], skiprows=1)
	taxa_num, align_len = [len(orig_align_df), orig_align_df["sequence"].str.split(' ').str.len().values[0]]
	# split sequences into alignment columns, one per site
	split_align_df = orig_align_df['sequence'].str.split(' ', expand=True).rename(columns = lambda x: str(x+1))
	
	# generate an empty output dataframe
	if args.ignore_leaf:
		# include the terminal sequence
		reconstructed_split_align_df = pd.DataFrame(0, index=internal_taxons + [args.ignore_leaf], columns=[])
	elif args.Homology:
		# include the terminal sequence
		reconstructed_split_align_df = pd.DataFrame(0, index=internal_taxons + [args.Homology], columns=[])
	else:
		# include only sequences from internal nodes
		reconstructed_split_align_df = pd.DataFrame(0, index=internal_taxons, columns=[]) 
	if args.print_posterior:
		Posterior_arr = []
	
	# read IRM, needs rows that sums to 0, also needs to be scaled 
	# so that it has on avg 1 AA state change per unit of time at equilibrium

	# overwrite state labels
	IRM_df = pd.read_csv(args.IRMfile, sep="\t", header=None, skiprows=1, names=definite_states, index_col=0)
	IRM_df.index = definite_states
	freqs_df = pd.read_csv(args.freqfile, sep="\t", header=None, skiprows=1, names=definite_states, index_col=None)
	nstates = len(definite_states)

	# from scaled IRM to Q with lines that sum to 0
	#exponentiate Q for every brlen in tree
	# head_node is the child node for this edge
	Pt_dict = {}
	IRM_arr = IRM_df.values
	for i in range(0,nstates):
		IRM_arr[i,i] = (np.sum(IRM_arr[i,:]) - IRM_arr[i,i])*-1	
	for edge in tree.postorder_edge_iter():
		if edge.length != None:
			Pt_df = pd.DataFrame(scipy.linalg.expm(edge.length* IRM_arr), columns=definite_states, index=definite_states)
			Pt_dict[edge.head_node.taxon.label] = Pt_df
			
	for site in [str(x) for x in range(1, align_len + 1)]:
		if args.exhaustive:
			obs_states = definite_states
		else:
			obs_states = []
			# handle mixed data, remove gaps
			for state in [s for s in split_align_df[site].unique() if s != '-']:
				if state in definite_states:
					obs_states.append(state)
				else:
					obs_states.extend(ambiguity_dict[state])
	
		# {node_name : {child_1_name : (p1, p2, p3, ..., pn), child2_name : (p1, p2, p3, ..., pn), parent_name : (p1, p2, p3, ..., pn))}}
		# Each node has two attributes after step 2: 
			# (1) the partial likelihood vector from child node 1, 
			# (2)  the partial likelihood vector from child node 2. 
		# These are labeled with the child node's name.
		# On the way down the tree a third parial likelihood vector, the one from the node sibling is computed.
		NODE_dict = defaultdict(dict)

		# step 1 (leaves)
		for leaf_node in tree.leaf_node_iter():
			child_name = leaf_node.taxon.label
			parent_name = leaf_node._parent_node.taxon.label

			# current state is always the observed state
			current_state = split_align_df[site][leaf_node.taxon.label]
			
			if args.Homology == leaf_node.taxon.label:
				# For Homology modelling don't ignore leaf, 
				# read as AA sequence
				current_state = masking_dict.get(current_state, "-")
			# get the probability matrix corresponding to the edge between leaf leaf_node and its parent
			Pt_df = Pt_dict[leaf_node.taxon.label]

			# the partial likelihood vector from leaf_node
			V = []
			# iterate over possible parent states
			for parent_state in obs_states:
				if args.ignore_leaf == leaf_node.taxon.label or current_state == '-':
					# compute transition probability from all possible states to current parent state
					# this means no information is obtained from the ignored (or gapped) sequence
					V.append(np.sum([Pt_df[possible_state][parent_state] for possible_state in obs_states]))
				elif current_state in ambiguous_states:
					# process ambiguous states
					# same as args.ignore_leaf BUT Pt summed over a set of ambiguous states
					# that map to current_state NOT over all possible states
					V.append(np.sum([Pt_df[possible_state][parent_state] for possible_state in ambiguity_dict[current_state]]))
				else:
					V.append(Pt_df[current_state][parent_state])
			# add V to the parent leaf_node and map it to the child leaf_node's name
			NODE_dict[parent_name][child_name] = np.array(V)
		
		#step 2 (all internal nodes)
		#each internal node is visited after its children (postorder traversal)
		for node in tree.postorder_internal_node_iter():
			if node._parent_node is None:
				root = node
				node_name = "ROOT"
			else:
				node_name = node.taxon.label
			childnames = [childnode.taxon.label for childnode in node.child_node_iter()]

			if args.Ignore_and_trim_sibling in childnames:
				# trim sibling by removing its name from childnames
				childnames = [args.Ignore_and_trim_sibling]
			
			for childname in childnames:
				# if child is internal (i.e. V from child has not been coputed in step 1),
				# compute partial likelihood vector from that child.
				# This is done by multiplying existing partial likelihood verctors for child and 
				# the transition probability vector from child to parent for all possible parent states
				if childname not in NODE_dict[node_name].keys():
					# retrive the  partial likelihood vectors from child
					child_V1, child_V2 = list(NODE_dict[childname].values())
					V = []
					# this Pt matrix corresponds to the edge between child node and current node
					Pt_df = Pt_dict[childname]

					for node_state in obs_states:
						# build a transition probability vector from child to node given node_state
						child_to_node_T = np.array([Pt_df[child_state][node_state] for child_state in obs_states])
						# element-wise product (hadamard product) of these three vectors
						V.append(np.sum(child_V1 * child_V2 * child_to_node_T))
					NODE_dict[node_name][childname] = np.array(V)
		
		# step 3 (assign root state)
		childnames = [childnode.taxon.label for childnode in root.child_node_iter()]
		# root can have 2 or 3 children
		root_V1, root_V2 = list(NODE_dict["ROOT"].values())[:2]
		# the posterior proobability vector is computed by multiplying (hadamard product)
		# the partial likelihood vectors coming from each child of root and the prior (i.e. the state equilibrium frequencies)
		prior_V = freqs_df[obs_states].values[0]
		Pv = root_V1 * root_V2 * prior_V 
		if len(childnames) == 3:
			root_V3 = list(NODE_dict["ROOT"].values())[-1]
			Pv = Pv * root_V3
		# normalize posterior
		Pv_sum = np.sum(Pv)
		Pv_norm = [P / Pv_sum for P in Pv.tolist()]
		# assign the state with highest posterior to root
		sorted_Pv_norm_dict = OrderedDict(sorted(dict(zip(obs_states, Pv_norm)).items(), key=lambda t:  t[1], reverse=True))
		reconstructed_split_align_df[site] = ['NA']*len(reconstructed_split_align_df)
		reconstructed_split_align_df[site]['ROOT'] = list(sorted_Pv_norm_dict.keys())[0]
		if args.print_posterior:
			sorted_d_str = json.dumps(sorted_Pv_norm_dict)
			rec_state = reconstructed_split_align_df[site]['ROOT']
			rec_state_P = sorted_Pv_norm_dict[rec_state]
			Posterior_arr.append(['ROOT', site, rec_state, rec_state_P, sorted_d_str])

		# step 4 (assign all other states)
		# each node is visited before its children
		nodes = [node for node in tree.preorder_internal_node_iter() if node is not root]
		if args.ignore_leaf:
			# also reconstruct ignored leaf
			nodes = nodes + [node for node in tree.leaf_node_iter() if args.ignore_leaf == node.taxon.label]
		elif args.Homology:
			# also reconstruct Homology leaf
			nodes = nodes + [node for node in tree.leaf_node_iter() if args.Homology == node.taxon.label]
		for node in nodes:
			node_name = node.taxon.label
			# this Pt matrix corresponds to the edge between current node and its parent node
			Pt_df = Pt_dict[node_name]
			if node._parent_node is root:
				parent_name = "ROOT"
			else:
				parent_name = node.parent_node.taxon.label
			# root can have 2 or 3 children,
			# for other internal nodes this fetches the sibling and the grandparent nodes
			relative_names = [k for k in NODE_dict[parent_name].keys() if k != node_name]
			if node_name == args.ignore_leaf or node_name == args.Homology:
				# a leaf has no children nodes
				Pv = prior_V
			else:
				# partial likelihood vectors from this node's children
				node_V1, node_V2 = list(NODE_dict[node_name].values())
				Pv = node_V1 * node_V2 * prior_V
			
			# the partial likelihood vector from all relative nodes
			all_relative_V = np.array([1] * len(obs_states))
			for relative_name in relative_names:
				relative_V = NODE_dict[parent_name][relative_name]
				all_relative_V = all_relative_V * relative_V
			
			# multiply for the transition probability from parent to current node
			V = []
			for node_state in obs_states:
				# build a transition probability vector from child to node given node_state
				parent_to_node_T = [Pt_df[parent_state][node_state] for parent_state in obs_states]
				# element-wise product (hadamard product) of these vectors
				V.append(np.sum(all_relative_V * parent_to_node_T))
			V = np.array(V) 
			NODE_dict[node_name][parent_name] = V
			Pv = Pv * V
			# normalize posterior
			Pv_sum = np.sum(Pv)
			Pv_norm = [P / Pv_sum for P in Pv.tolist()]
			# sort states by likelihood
			sorted_Pv_norm_dict = OrderedDict(sorted(dict(zip(obs_states, Pv_norm)).items(), key=lambda t:  t[1], reverse=True))
			
			if args.Homology == node_name:
				observed_state = split_align_df[site][node_name]
				
				if observed_state == '-':
					sorted_Pv_norm_dict = {"-" : 1.}
				
				# for Homology modelling constrain state selection to those rotamer states that map to observed AA
				possible_states = ambiguity_dict.get(masking_dict.get(observed_state, "-"), ["-"])
				possible_states_dict = dict([(k, sorted_Pv_norm_dict[k]) for k in possible_states])
				sorted_Pv_norm_dict = OrderedDict(sorted(possible_states_dict.items(), key=lambda t:  t[1], reverse=True))
			
			# assign most likely state to site
			reconstructed_split_align_df[site][node_name] = list(sorted_Pv_norm_dict.keys())[0]
			
			if args.print_posterior:
				sorted_d_str = json.dumps(sorted_Pv_norm_dict)
				rec_state = reconstructed_split_align_df[site][node_name]
				rec_state_P = sorted_Pv_norm_dict[rec_state]
				Posterior_arr.append([node_name, site, rec_state, rec_state_P, sorted_d_str])

	if args.print_posterior:
		Posterior_df = pd.DataFrame(Posterior_arr, 
				columns=["taxon", "site", 
					"rec_state", "rec_state_posterior", 
					"posterior_dict"])
		Posterior_df.to_csv(args.print_posterior, 
				header=True, index=False, 
				sep="\t")

	reconstructed_split_align_df['sequences'] = reconstructed_split_align_df.iloc[:,0:align_len].apply(lambda x: ' '.join(x), axis=1)
	reconstructed_split_align_df['taxons'] = reconstructed_split_align_df.index
	

	if args.ignore_leaf:
		print("{}\t{}".format(args.ignore_leaf, reconstructed_split_align_df.loc[args.ignore_leaf]['sequences']))	
	elif args.Homology:
		print("{}\t{}".format(args.Homology, reconstructed_split_align_df.loc[args.Homology]['sequences']))	
	else:
		print("{}\t{}".format(len(reconstructed_split_align_df), align_len))
		print(reconstructed_split_align_df.to_csv(header=False, index=False, sep="\t", columns=['taxons', 'sequences']))
	
if __name__ == '__main__':
	main()

